const express = require('express');
const mongoose = require('mongoose');
const Joi = require('joi');
const app = express();
const allRouter = require('./routes/allRouter')

mongoose.set("strictQuery", false);
mongoose.connect('mongodb://localhost:27017', { useNewUrlParser: true })
const conn = mongoose.connection;


conn.on('open', () => {
    console.log('Database connected');
})

app.use(express.json());
app.use('/api', allRouter);



const port = process.env.PORT || 3000;
app.listen(port, () => { console.log(`Ready on port ${port}`); });