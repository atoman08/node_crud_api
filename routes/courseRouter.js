const { json } = require('express');
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const Courses = require('../model/courseModel');


router.get('/', async(req, res) => {
    try {
        //code to handle
        let courses = await Courses.find();
        res.send(courses)
    } catch (error) {
        //error here
        res.status(500).send(`Error: ${error.message}`);
    }

})


// Get a single course
router.get('/:id', async(req, res) => {
    try {
        //code to handle
        let course = await Courses.findById(req.params.id);
        if (!course) return res.status(404).send(`Course not found`);

        res.json(course);
    } catch (error) {
        //error here
        res.status(500).send(`Error: ${error.message}`);
    }

});


//Add courses to Database
router.post('/', async(req, res) => {

    //code to handle
    const schema = Joi.object({
        name: Joi.string().required(),
        category: Joi.string().required(),
        status: Joi.boolean().required(),
        desc: Joi.string()
    });

    let validation = await schema.validate(req.body);

    if (validation.error) return res.status(400).send(explodeValidation(validation.error.details));

    let course = new Courses({
        name: req.body.name,
        category: req.body.category,
        status: req.body.status,
        desc: req.body.desc
    });

    try {

        let result = await course.save();
        return res.json(result);

    } catch (error) {
        //error here
        res.status(500).send(`Error: ${error.message}`);
    }

});

// Explode Validation Errors into String Messages
const explodeValidation = (errors = []) => {
    if (errors.length === 0) return null;

    let validatedErrors = 'Validation: ';

    for (let i = 0; i < errors.length; i++) {
        if (i === (errors.length - 1)) {
            validatedErrors += errors[i].message;
        } else {
            validatedErrors += `, ${errors[i].message}`;
        }

    }
    return validatedErrors;
}



module.exports = router;