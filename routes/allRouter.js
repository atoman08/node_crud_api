const express = require('express')
const router = express.Router();
const courseRouter = require('./courseRouter');


router.use('/courses', courseRouter);


module.exports = router;