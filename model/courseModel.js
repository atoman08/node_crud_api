const { string } = require('joi');
const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
    name: { type: String, required: true },
    category: { type: String, required: true },
    status: { type: Boolean, required: true, default: false },
    desc: { type: String, default: null }
});

module.exports = mongoose.model('Courses', courseSchema);